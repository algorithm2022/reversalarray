/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reversalmidterm;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class reversalArray {
    public static void runtime(int n){
        long sum = 0;
        for(int i=1;i<=n;i++){
            for(int j=1;j<=2;j++){
                sum++;
            }
        }
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        long start, stop;
        start = System.nanoTime();
        
        int[] arr = new int[10];
        
        System.out.print("Input an array ");
        for(int i=0; i < arr.length;i++){
           arr[i] = kb.nextInt();
       }
        System.out.println();
        System.out.print("Your array input is: ");
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        System.out.print("Your reverse array is: ");
        for(int i=arr.length-1;i>=0;i--){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
        runtime(50);
        stop = System.nanoTime();
        System.out.println("Runtime of your algorithm: "+ (stop-start) * 1E-9 +" secs.");
    }
    
}
